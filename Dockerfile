FROM node:4.0.0

RUN mkdir -p /usr/src/app
RUN useradd -ms /bin/bash node
RUN cd && cp -R .bashrc .profile /home/node

WORKDIR /usr/src/app
COPY . /usr/src/app

RUN chown -R node:node /usr/src
USER node
ENV HOME /home/node

CMD ["npm", "start"]

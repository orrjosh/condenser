"use strict";
let startWebServer = function(){
  var express = require('express')
  var app = express()
  var bodyParser = require('body-parser')
  app.use(bodyParser.json())

  var routes = require('./app/routes')

  routes(app)
  app.listen(8901, function(){
    console.log("Starting web server...")
  })
}

startWebServer()

"use strict";

var polyHandler = require('./polyhandler')
var endpoints = require('../config/config.json')

let applyTo = function(app) {
  endpoints.forEach(function(currentValue,index,arr){
    app.get("/"+currentValue.endpoint, polyHandler.requestHandler)
   
  });
}

module.exports = applyTo

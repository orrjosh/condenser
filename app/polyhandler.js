"use strict";

var endpoints = require('../config/config.json')
var request = require('request')
var async = require('async')

let requestHandler = function(req, response){
  var endpoint = endpointSearch(endpoints, req.url);
  if(endpoint == null){
    response.send(404)
  }
  let callFunctions = []
  endpoint.downstream_endpoints.forEach(function(currentValue, index, arr){
    let callFunction = function(callback){
      if(endpoint.forward_headers){
        currentValue.options.headers = req.headers
      }
      request(currentValue.options, function(err,response,body){
        if(err){
          console.log(err);
          callback(true);
          return;
        }
        var obj = JSON.parse(body)
        callback(false,obj)
      });
    };
    callFunctions.push(callFunction)
  });
  async.parallel(callFunctions, function callback(error, results){
    if(error){
      console.log("Error"+error);
      response.status(500).send("Server Error")
      return;
    }
    response.send(results);
  });
}

let endpointSearch = function(endpointArray, requestURL){
  if(requestURL.indexOf('/') === 0){
    requestURL = requestURL.replace('/','');
  }
  for(var i=0;i<endpointArray.length;i++){
    if(endpointArray[i].endpoint == requestURL){
      return endpointArray[i]
    }
  }
  return null;
}

let isJsonString = function(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}
exports.requestHandler = requestHandler
